Role Name
=========

A role that installs and configures Cassandra, <https://cassandra.apache.org/>

Role Variables
--------------

The most important variables are listed below:

``` yaml
cassandra_install_from_external_repo: true
cassandra_install_packages: true
cassandra_latest_version: true
cassandra_start_server: true
cassandra_cluster_enabled: false
cassandra_cluster_port: 7000
cassandra_cluster_ssl_port: 7001
cassandra_native_transport_port: 9042
cassandra_rpc_port: 9160
cassandra_jmx_port: 7199
cassandra_allowed_hosts:
  - 127.0.0.1/8
  - '{{ ansible_default_ipv4.address }}/32'
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
